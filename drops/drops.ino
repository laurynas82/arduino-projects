//We always have to include the library
#include "LedControl.h"

/*
  Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
  pin 12 is connected to the DataIn
  pin 11 is connected to the CLK
  pin 10 is connected to LOAD
  We have only a single MAX72XX.
*/

/*

lc.setRow(x, y, leds); // on device x, row y, print binnary value leds of 8 possitions
lc.setColumn(x, y, leds);// on device x, column y, print binnary value leds of 8 possitions
lc.setLed(x, row, col, true);

*/
LedControl lc = LedControl(12, 11, 10, 1);
const int analogInPin = A1; // Analog input pin that the potentiometer is attached to

/* we always wait a bit between updates of the display */
unsigned long delaytime = 100;
unsigned long readValueDelay = 1000;
int sensorValue = 0; // value read from the pot

byte IMAGES[][8] = {
    {0x60, 0x60, 0xF0, 0xF2, 0x62, 0x7, 0x7, 0x2},
    {0x0, 0x60, 0x60, 0xF0, 0xF2, 0x62, 0x7, 0x7},
    {0x2, 0x0, 0x60, 0x60, 0xF0, 0xF2, 0x62, 0x7},
    {0x7, 0x2, 0x0, 0x60, 0x60, 0xF0, 0xF2, 0x62},
    {0x67, 0x7, 0x2, 0x0, 0x60, 0x60, 0xF0, 0xF2},
    {0xF2, 0x67, 0x7, 0x2, 0x0, 0x60, 0x60, 0xF0},
    {0xF2, 0xF2, 0x67, 0x7, 0x2, 0x0, 0x60, 0x60},
    {0x60, 0xF2, 0xF2, 0x67, 0x7, 0x2, 0x0, 0x60}};

byte happy[8] = {
    0x42, 0xA5, 0x0, 0x18, 0x18, 0xC3, 0x3C, 0x0};

byte sad[8] = {
    0x0, 0xA5, 0x42, 0x18, 0x58, 0x0, 0x7E, 0x81};

byte normal[8] = {
    0x0, 0x66, 0x0, 0x18, 0x18, 0x81, 0x7E, 0x0};

void setup()
{
  /*
    The MAX72XX is in power-saving mode on startup,
    we have to do a wakeup call
  */
  lc.shutdown(0, false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0, 8);
  /* and clear the display */
  lc.clearDisplay(0);
}

void writeDrops()
{

  int numFrame = sizeof(IMAGES) / 8;

  for (byte frame = 0; frame < numFrame; frame++)
  {
    for (byte row = 0; row < 8; row++)
    {
      lc.setColumn(0, row, IMAGES[frame][7 - row]);
    }
    delay(delaytime);
  }
}

void showIcon(byte *icon)
{
  for (byte row = 0; row < 8; row++)
  {
    lc.setColumn(0, row, icon[7 - row]);
  }
}

void loop()
{
  //  writeDrops();

  sensorValue = analogRead(analogInPin);

  if (sensorValue < 300)
  {
    showIcon(sad);
    delay(readValueDelay);
  }
  else if (sensorValue < 500)
  {
    showIcon(normal);
    delay(readValueDelay);
  }
  else if (sensorValue < 700)
  {
    showIcon(happy);
    delay(readValueDelay);
  }
  else
  {
    writeDrops();
  }
}
